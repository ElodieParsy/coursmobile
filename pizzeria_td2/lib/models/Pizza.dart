class Pizza{
  final int id;
  final String title;
  final String garniture;
  final String image;
  final double price;

  Pizza(this.id, this.title, this.garniture, this.image, this.price);
}