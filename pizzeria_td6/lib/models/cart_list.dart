import 'package:flutter/material.dart';
import 'package:intl/src/intl/number_format.dart';
import 'package:pizzeria/models/cart.dart';
import 'package:pizzeria/ui/share/pizzeria_style.dart';
import 'package:provider/src/provider.dart';


class CartList extends StatelessWidget {
  var format = NumberFormat("###.00 €");

  @override
  Widget build(BuildContext context) {
    var cart = context.watch<Cart>();

    return ListView.builder(
        padding: const EdgeInsets.all(8.0),
        itemCount: cart.totalItems(),
        itemBuilder: (context, index) {
          return _buildItem(cart.getCartItem(index), cart);
        });
  }


  Widget _buildItem(CartItem cartItem, Cart cart) {
    return Row(
      children: [
        Image.network(
          cartItem.pizza.image,
          height: 180,
        ),
        Column(
          children: [
            Text(
              cartItem.pizza.title,
              style: PizzeriaStyle.pageTitleTextStyle,
            ),
            Row(
              children: [
                Text('${cartItem.pizza.total.toString()} €'),
                IconButton(
                  onPressed: () {
                    cart.removeProduct(cartItem.pizza);
                    print("on enlève un élément");
                  },
                  icon: Icon(Icons.remove),
                ),
                Text(cartItem.quantity.toString()),
                IconButton(
                  onPressed: () {
                    cart.addProduct(cartItem.pizza);
                    print("on ajoute un élément");
                  },
                  icon: Icon(Icons.add),
                ),
              ],
            ),
            Text(
              'Sous-Total: ${cartItem.pizza.total * cartItem.quantity} €',
              style: PizzeriaStyle.subPriceTextStyle,
            ),

          ],
        )
      ],
    );
  }
}

