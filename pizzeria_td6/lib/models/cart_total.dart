import 'package:flutter/material.dart';
import 'package:pizzeria/models/cart.dart';
import 'package:pizzeria/ui/share/pizzeria_style.dart';
import 'package:provider/provider.dart';
import 'package:intl/src/intl/number_format.dart';



class CartTotal extends StatelessWidget {
  var format = NumberFormat("###.00 €");
  @override
  Widget build(BuildContext context) {

    return Container(
        padding: EdgeInsets.all(12.0),
        height: 220,
        child: Consumer<Cart>(builder: (context, cart, child) {
          final double _total = cart.totalPrice();
          final double _totalTVA = cart.totalPriceTVA();
          final double _totalTTC = cart.totalPriceTTC();

          if (_total == 0) {
            return Center(
              child: Text(
                'Aucun produit',
                style: PizzeriaStyle.priceTotalTextStyle,
              ),
            );
          } else {
            return Column(children: [
              Container(
                margin: EdgeInsets.all(20),
                child: Table(
                  defaultColumnWidth: FixedColumnWidth(120.0),
                  border: TableBorder.all(
                      color: Colors.blueGrey,
                      style: BorderStyle.solid,
                      width: 2),
                  children: [
                    TableRow( children: [
                      Column(children:[]),
                      Column(children:[Text('TOTAL HT : ', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                      Column(children:[Text('${_total}', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                    ]),
                    TableRow( children: [
                      Column(children:[]),
                      Column(children:[Text('TVA', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                      Column(children:[Text('${_totalTVA}', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                    ]),
                    TableRow( children: [
                      Column(children:[]),
                      Column(children:[Text('TOTAL TTC', style: PizzeriaStyle.subPriceTextStyle)]),
                      Column(children:[Text('${_totalTTC}', style: PizzeriaStyle.subPriceTextStyle)]),
                    ]),
                  ],
                ),
              ),
              Container(
                child:
                SizedBox(
                  width: double.infinity,

                  child:ElevatedButton(
                    child:
                    Text('Valider le panier'),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.red.shade800)),
                    onPressed: () {
                      print('Valider');
                    },
                  ),
                ),
              ),
            ]);
          }
        }));
  }
}
