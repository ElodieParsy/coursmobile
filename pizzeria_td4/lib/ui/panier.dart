import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pizzeria/models/cart.dart';
import 'package:pizzeria/ui/share/pizzeria_style.dart';


class Panier extends StatefulWidget {
  final Cart _cart;

  const Panier(this._cart, {Key? key}) : super(key: key);

  @override
  _PanierState createState() => _PanierState();
}

class _PanierState extends State<Panier> {
  @override
  Widget build(BuildContext context) {
    var format = NumberFormat("###.00 €");

    String totalPrice = format.format(widget._cart.totalPrice());
    String totalPriceTVA = format.format(widget._cart.totalPriceTVA());
    String totalPriceTTC = format.format(widget._cart.totalPriceTTC());
    return Scaffold(
      appBar: AppBar(
        title: Text(' Mon panier'),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(8.0),
                // itemExtent: ,
                itemCount: widget._cart.totalItems(),
                itemBuilder: (context, index) {
                  return _buildItem(widget._cart.getCartItem(index));
                }),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: Table(
              defaultColumnWidth: FixedColumnWidth(120.0),
              border: TableBorder.all(
                  color: Colors.blueGrey,
                  style: BorderStyle.solid,
                  width: 2),
              children: [
                TableRow( children: [
                  Column(children:[]),
                  Column(children:[Text('TOTAL HT : ', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                  Column(children:[Text('${totalPrice}', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                ]),
                TableRow( children: [
                  Column(children:[]),
                  Column(children:[Text('TVA', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                  Column(children:[Text('${totalPriceTVA}', style: PizzeriaStyle.priceSubTotalTextStyle)]),
                ]),
                TableRow( children: [
                  Column(children:[]),
                  Column(children:[Text('TOTAL TTC', style: PizzeriaStyle.subPriceTextStyle)]),
                  Column(children:[Text('${totalPriceTTC}', style: PizzeriaStyle.subPriceTextStyle)]),
                ]),
              ],
            ),
          ),
          Container(
            child:
            SizedBox(
              width: double.infinity,

              child:ElevatedButton(
                child:
                Text('Valider le panier'),
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.red.shade800)),
                onPressed: () {
                  print('Valider');
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(CartItem cartItem){
    return Row(
      children: [
        Image.asset(
          'assets/images/pizzas/${cartItem.pizza.image}',
          height: 180,
        ),
        Column(
          children: [
            Text(
              cartItem.pizza.title,
              style: PizzeriaStyle.pageTitleTextStyle,
            ),
            Row(
              children: [
                Text('${cartItem.pizza.total.toString()} €'),
                IconButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => this.widget));
                    widget._cart.removeProduct(cartItem.pizza);
                    print("on enlève un élément");
                  },
                  icon: Icon(Icons.remove),
                ),
                Text(cartItem.quantity.toString()),
                IconButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => this.widget));
                    widget._cart.addProduct(cartItem.pizza);
                    print("on ajoute un élément");
                  },
                  icon: Icon(Icons.add),
                ),
              ],
            ),
            Text(
              'Sous-Total: ${cartItem.pizza.total * cartItem.quantity} €',
              style: PizzeriaStyle.subPriceTextStyle,
            ),

          ],
        )
      ],
    );
  }


}
