import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pizzeria/models/Pizza.dart';
import 'package:flutter/material.dart';

class PizzeriaService{
  static final String uri = 'http://10.0.2.2/api/';

  Future<List<Pizza>> fetchPizzas() async{
    List<Pizza> list = [];

    try{
      // appel http bloquant
      final response = await http.get(Uri.parse('${uri}/pizzas'));

      if(response.statusCode == 200){
        // var json = jsonDecode(response.body);
        // --> problèmes avec less accents
        // décodage des accents : utf8.decode
        // decodage json : jsonDecode
        var json = jsonDecode(utf8.decode(response.bodyBytes));
        for (final value in json){
          // création de l'objet pizza avec le json :
          // Pizza.fromJson(json)
          //puis ajout dans la liste
          list.add(Pizza.fromJson(value));
        }
      }else{
        throw Exception('Impossible de récupérer les pizzas');
      }
    } catch(e){
      throw e;
    }
    return list;
  }
}