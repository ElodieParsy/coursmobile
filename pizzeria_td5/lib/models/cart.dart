import 'package:pizzeria/models/Pizza.dart';

class CartItem{
  final Pizza pizza;
  int quantity;

  CartItem(this.pizza, [this.quantity = 1]);
}

class Cart{
  List<CartItem> _items = [];

  int totalItems(){ return _items.length; }
  CartItem getCartItem(int index){
    return _items[index];
  }


  double totalPrice() {
    var prix = 0.0;
    for (var element in _items) {
      prix = prix + (element.pizza.total * element.quantity);
    }
    return prix;
  }

  double totalPriceTVA() {
    var prix = totalPrice();
    return prix * (20 / 100);
  }

  double totalPriceTTC() {
    var prix = totalPrice();
    var tva = totalPriceTVA();
    prix = prix + tva;
    return prix;
  }

  void addProduct(Pizza pizza){
    // Recherche du produit
    int index = findCartItem(pizza.id);
    if(index == -1){
      // Ajout
      _items.add(CartItem(pizza));
    }else{
      // Incrémente la quantité
      CartItem item = _items[index];
      item.quantity++;
    }
  }

  /*void removeProduct(Pizza pizza){
    // Recherche du produit
    int index = findCartItem(pizza.id);
    if(index != -1){
      // Suppression
      _items.removeAt(index);
    }
  }*/

  void removeProduct(Pizza pizza){
    // Recherche du produit
    int index = findCartItem(pizza.id);
    if(index != -1){
      // Décrémente la quantité
      CartItem item = _items[index];
      if(item.quantity == 1){
        _items.removeAt(index);
      }else{
        item.quantity--;
      }
    }
  }

  int findCartItem(int id){
    return _items.indexWhere((element) => element.pizza.id == id);
  }
}